const poll = (action, condition, timeout = 5000) => {
    return new Promise((resolve, reject) => {
        const actionThatHappensAfterTimeout = setTimeout(() => {
            clearInterval(actionThatRepeats);
            reject();
        }, timeout);

        const actionThatRepeats = setInterval(() => {
            console.log('Performing action() ...');
            action()
            .then((res) => {
                if(condition(res)) {
                    console.log('\tSuccess condition is met ...');
                    clearTimeout(actionThatHappensAfterTimeout);
                    clearInterval(actionThatRepeats);
                    resolve(res);
                }
            })
            .catch(() => {
                // Leeway for action() to resolve in the next tick
                console.log('\tSuccess condition is still not met ...');

            });
        }, 1000);
    });
}

module.exports = { poll };