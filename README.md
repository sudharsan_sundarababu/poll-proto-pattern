# Poll

A promise based poll proto-pattern in node.

## Prerequisites
node version 6.2.0, npm version 3.8.9

## Installation
```
npm install
```
## Run
```
node usePoll.js
```

