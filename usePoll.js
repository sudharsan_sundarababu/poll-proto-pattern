const { poll } = require('./poll');

const action = () => {
    const rand = (Math.random() * 1000) % 10;
    if (rand >= 4 ) return Promise.resolve(100);
    return Promise.reject();
}

const condition = (val) => {
    return (val == 100);
}

const timeout = 10000;

return poll(action, condition, timeout)
    .catch((e) => {})